<?php
 include ('includes/header.php');
 include ('setup/conexion.php');
 include ('setup/helpers.php');
 
?>
	
	<section class="hero-section">
	
		<div class="hero-slider owl-carousel">
			<div class="hero-slide-item set-bg" data-setbg="assets/img/slider-bg-1.jpg">
				<div class="hs-text">
					<h2 class="hs-title-1"><span>Recetas Saludables</span></h2>
					<h2 class="hs-title-2"><span>De los mejores chefs</span></h2>
					<h2 class="hs-title-3"><span>Para todos los amantes de la comida</span></h2>
				</div>
			</div>
			<div class="hero-slide-item set-bg" data-setbg="assets/img/slider-bg-2.jpg">
				<div class="hs-text">
					<h2 class="hs-title-1"><span>Recetas Saludables</span></h2>
					<h2 class="hs-title-2"><span>De los mejores chefs</span></h2>
					<h2 class="hs-title-3"><span>Para todos los amantes de la comida</span></h2>
				</div>
			</div>
		</div>
	</section>
	
	
							<?php if(isset( $_SESSION['error_login'])):?>
							<div class="alert alert-danger" role="alert">
									<?=$_SESSION['error_login']?>
								
							</div>
							<?php endif?>


	<section class="add-section spad">
		<div class="container">
			<div class="add-warp">
				<div class="add-slider owl-carousel">
					<div class="as-item set-bg" data-setbg="assets/img/add/1.jpg"></div>
					<div class="as-item set-bg" data-setbg="assets/img/add/2.jpg"></div>
					<div class="as-item set-bg" data-setbg="assets/img/add/3.jpg"></div>
				</div>
				<div class="row add-text-warp">
					<div class="col-lg-4 col-md-5 offset-lg-8 offset-md-7">
						<div class="add-text text-white">
							<div class="at-style"></div>
							
							<?php if(!isset($_SESSION['usuario'])):?>
							<h2>Registrate o logueate</h2>
							<ul class="main-menu2">
							<li><a href="login.view.php">Login/register</a></li>
							</ul>
							<?php endif?>
							<?php if(isset($_SESSION['usuario'])):?>
							<div>
								<h2>Bienvenido <br><?= $_SESSION['usuario']['NOMBRE'].' '.$_SESSION['usuario']['APELLIDO'];?></h2>
							<a href="mis-datos.php"><button type="button" class="btn btn-success">Mis datos</button></a>

							<a href="crear-entradas.php"><button type="button" class="btn btn-primary">Crear recetas</button></a>

							<a href="crear-categorias.php"><button type="button" class="btn btn-secondary">Crear categorias</button></a><br>

							<a href="cerrar.php"><button type="button" class="btn btn-warning">Cerrar sesion</button></a>
							</div>
							<?php endif?>
							<div id="botonesinfo">
							
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Add section end -->
<?php if(isset($_SESSION['usuario'])):?>	
	<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<h3>Categorias</h3>
			<ul class="main-menu1">
				<?php $sql = "SELECT * FROM categorias ORDER BY nombre DESC";
    			$categorias=mysqli_query($db, $sql);
   				 $result=array();
    			if ($categorias && mysqli_num_rows($categorias) >= 1) {
        		$result=$categorias;
    			}?>
					
				
					<?php   
					while($categoria = mysqli_fetch_assoc($categorias)):
					
					?>
					
					<li>	
					<a href="categoria.php?id=<?=$categoria['ID']?>"><?=$categoria['NOMBRE']?></a>
					</li>
						
					<?php endwhile;
						
					?>
				

				</ul>
			</div>

		</div>
	</div>
	</section>
	

	<!-- Recipes section -->
	<section class="recipes-section spad pt-0">
		<div class="container">
			<div class="section-title">
				<h2>Ultimas Recetas</h2>
				
			</div>
			<div class="row">
			<?php 
				$sql = "SELECT e.*, c.NOMBRE AS 'categoria'FROM ENTRADAS e INNER JOIN CATEGORIAS c ON e.ID = c.ID ORDER BY e.ID ASC LIMIT 6";
				$entradas=mysqli_query($db, $sql);
				$resultado=array();
				if($entradas && mysqli_num_rows($entradas) >=1){
					$resultado = $entradas;
				}?>
				<?php 
				while($entrada = mysqli_fetch_assoc($entradas)):
				?>	
				<a href="detalle.php?id=<?=$entrada['ID']?>">
					<div class="col-lg-4 col-md-6">
						<div class="recipe">
							<img src="assets/img/recipes/<?php echo $entrada['THUB']?>" alt="">
							<div class="recipe-info-warp">
								<div class="recipe-info">
									<h2 id="title1"><?= $entrada['TITULO']?></h2>
									
									<p><?= $entrada['PREVIEW']?></p>
									<small id="promblems"><?= $entrada['FECHA']?></small>
									<p><?=$entrada['categoria']?></p>
										
									
								</div>
							</div>
						</div>
						</a>
					</div>
				<?php 
					endwhile;
				?>
				
			</div>
			<div class="row">
					<div class="col-md-12 text-center boton">
					<a href="recetas.php"> <button type="button" class="btn ">Mostrar todas las recetas</button></a>
					</div>
			</div>
		</div>
	</section>
	<!-- Recipes section end -->
	<?php endif?>

	


	<!-- Review section end -->
	<section class="review-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-8 offset-lg-0 offset-md-2">
					<div class="review-item">
						<div class="review-thumb set-bg" data-setbg="assets/img/thumb/11.jpg">
							
						</div>
						<div class="review-text">
							
							<h3>Comidas sanas</h3>
							<div class="author-meta">
								<div class="author-pic set-bg" ></div>
								<p>Comience su vida saludable hoy!</p>
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-8 offset-lg-0 offset-md-2">
					<div class="review-item">
						<div class="review-thumb set-bg" data-setbg="assets/img/thumb/12.jpg">
							
						</div>
						<div class="review-text">
							
							<h3>Pastas frescas</h3>
							
							<div class="author-meta">
								<div class="author-pic set-bg" ></div>
								<p>Realiza tus propias pastas!</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<div class="gallery">
		<div class="gallery-slider owl-carousel">
			<div class="gs-item set-bg" data-setbg="assets/img/instagram/1.jpg"></div>
			<div class="gs-item set-bg" data-setbg="assets/img/instagram/2.jpg"></div>
			<div class="gs-item set-bg" data-setbg="assets/img/instagram/3.jpg"></div>
			<div class="gs-item set-bg" data-setbg="assets/img/instagram/4.jpg"></div>
			<div class="gs-item set-bg" data-setbg="assets/img/instagram/5.jpg"></div>
			<div class="gs-item set-bg" data-setbg="assets/img/instagram/6.jpg"></div>
		</div>
	</div>
	

<?php
 include ('includes/footer.php')
?>
	