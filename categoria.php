
<?php
 include ('includes/header.php');
 include ('setup/conexion.php');
 include ('setup/helpers.php');
?>
<?php 
				$id=$_GET['id'];
				$sql = "SELECT * FROM categorias WHERE ID=$id";
				//var_dump(mysqli_error($db));
				$categorias=mysqli_query($db, $sql);
				
				$resultado=mysqli_fetch_assoc($categorias);
				if (!isset($resultado['ID'])) {
					header('Location: index.php');
				}
?>
   <section class="recipes-section spad pt-0">
   				
		<div class="container">
			<div class="section-title">
				<h2>Recetas de <?php echo $resultado['NOMBRE']?></h2>
				
			</div>
			<div class="row">
			<?php 
				$id=$_GET['id'];
				$sql = "SELECT e.*, c.NOMBRE AS 'categoria'FROM ENTRADAS e INNER JOIN CATEGORIAS c ON e.FKCATEGORIAS = c.ID WHERE e.FKCATEGORIAS = $id ORDER BY e.ID ASC ";
				$entradas=mysqli_query($db, $sql);
				$resultado=array();
				if($entradas && mysqli_num_rows($entradas) >=1){
					$resultado = $entradas;
				}?>
				<?php 
				while($entrada = mysqli_fetch_assoc($entradas)):
				?>	
				<a href="detalle.php?id=<?=$entrada['ID']?>">
					<div class="col-lg-4 col-md-6">
						<div class="recipe">
							<img src="assets/img/recipes/<?php echo $entrada['THUB']?>" alt="">
							<div class="recipe-info-warp">
								<div class="recipe-info">
									<h2 id="title1"><?= $entrada['TITULO']?></h2>
									
									<p><?= $entrada['PREVIEW']?></p>
									<small><?= $entrada['FECHA']?></small>
									<p><?=$entrada['categoria']?></p>
										
									
								</div>
							</div>
						</div>
						</a>
					</div>
				<?php 
					endwhile;
				?>
				
				
			</div>
			<div class="row">
					<div class="col-md-12 text-center boton">
					
					</div>
			</div>
		</div>
	</section>



<?php
 include ('includes/footer.php')
?>