<?php
 include ('includes/header.php');
 include ('setup/conexion.php');
 include ('setup/helpers.php');
?>
    <section class="recipes-section spad pt-0">
		<div class="container">
			<div class="section-title">
				<h2>Todas las recetas</h2>
				
			</div>
			<div class="row">
			<?php 
				$sql = "SELECT * FROM ENTRADAS";
				$entradas=mysqli_query($db, $sql);
				$resultado=array();
				if($entradas && mysqli_num_rows($entradas) >=1){
					$resultado = $entradas;
				}?>
				<?php 
				while($entrada = mysqli_fetch_assoc($entradas)):
				?>	
				<a href="detalle.php?id=<?=$entrada['ID']?>">
					<div class="col-lg-4 col-md-7">
						<div class="recipe">
							<img src="assets/img/recipes/<?php echo $entrada['THUB']?>" alt="">
							<div class="recipe-info-warp">
								<div class="recipe-info">
									<h2 id="title1"><?= $entrada['TITULO']?></h2>
									
									<p><?= $entrada['PREVIEW']?></p>
									<small id="promblems"><?= $entrada['FECHA']?></small>
										
									
								</div>
							</div>
                        </div>
                         </a>
                    </div>
                   
				<?php 
					endwhile;
				?>
				
			</div>
			
		</div>
	</section>



<?php
 include ('includes/footer.php')
?>