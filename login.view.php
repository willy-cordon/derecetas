<?php
 include ('includes/header.php');
 include ('setup/conexion.php');
 include ('setup/helpers.php');
?>

<!--h1 class="text-center">Vertical layout</h1-->
<div class="container pt-3">
  <div class="row justify-content-sm-center">
    <div class="col-sm-6 col-md-4">
    
      <div class="card border-info text-center">
        <div class="card-header">
          <h2>Inicie sesión</h2>
        </div>
        <?php if(isset($_SESSION['completado'])):?>
									<div class="alert alert-success" role="alert">
										<?=$_SESSION['completado']?>
									</div>
								<?php elseif (isset($_SESSION['error']['general'])):?>	
								<div class="alert alert-danger" role="alert">
									<?=$_SESSION['error']['general']?>
								</div>
							<?php endif?>
        <div class="card-body">
        <?php if(isset($_SESSION['usuario'])):?>
        <img src="" alt="">
        
        <?php endif?>
          <img src="">
          
          <form action="login.php" method="POST" class="form-signin">
            <input type="text" class="form-control mb-2" placeholder="Email" required autofocus name="email">
            <input type="password" class="form-control mb-2" placeholder="Password" required name="password">
            <button class="btn btn-lg btn-primary btn-block mb-1" type="submit">Iniciar Sesión</button>
            
          </form>
        </div>
      </div>
      <a href="registro.view.php" class="float-right">Crear cuenta </a>
    </div>
  </div>
</div>
<?php
 include ('includes/footer.php')
?>