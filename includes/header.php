<?php if (!isset($_SESSION)) {
    session_start();
}?>

<!DOCTYPE html>
<html lang="es/en">
<head>
	<title>willy-cordon</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="assets/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="assets/css/owl.carousel.css"/>
	<link rel="stylesheet" href="assets/css/animate.css"/>
	<link rel="stylesheet" href="assets/css/style.css"/>


	

</head>
<body>
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<header class="header-section">
		<div class="header-top">
			<div class="container">
				
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
				<a href="index.php" class="site-logo">
					<h2>Blog de comida</h2>
				</a>
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				<?php if(isset($_SESSION['usuario'])):?>
				<div class="header-search">
					<a href="#"><i class="fa fa-search"></i></a>
				</div>
				
				<ul class="main-menu">
					<li><a href="index.php">Home</a></li>
					<li><a href="recetas.php">Recetas</a></li>
					<li><a href="cerrar.php">Cerrar sesión</a></li>
				</ul>
				<?php endif?>
			</div>
		</div>
	</header>