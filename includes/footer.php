
<footer class="footer-section set-bg" >
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<h2>Blog de comida</h2>
					</div>
					<p>En la actualidad se ha establecido que lo más saludable es realizar de 3 a 5 comidas al día, con el fin de mantener los niveles de azúcar en la sangre regulados. Existen tres tiempos de comida que no debemos omitir por salud, es decir siempre debemos desayunar, almorzar y cenar, realizando uno o dos tiempos de comida intermedios para lograr este objetivo.</p>
				</div>
				<div class="col-lg-6 text-lg-right">
					<ul class="footer-menu">
					<li><a href="/">Home</a></li>
					<li><a href="#">Recetas</a></li>
					<li><a href="#">Contacto</a></li>
					<li><a href="login.view.php">Login/register</a></li>
					</ul>
					<p class="copyright">
					Copyright &copy; willy cordon - Escuela davinci 2018

</p>
				</div>
			</div>
		</div>
	</footer>

	<script src="assets/js/jquery-3.2.1.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>
	<script src="assets/js/main.js"></script>
</body>
</html>