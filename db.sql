DROP DATABASE IF EXISTS dw3_cordon_willy;
CREATE DATABASE dw3_cordon_willy;
USE dw3_cordon_willy;


CREATE TABLE categorias(
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    NOMBRE VARCHAR(60)
)ENGINE=innoDB;

CREATE TABLE sexos( 
	IDSEXO tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	SEXO VARCHAR(12) UNIQUE NOT NULL 
)ENGINE=innoDB;

CREATE TABLE usuarios( 
	ID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	NOMBRE VARCHAR(60),
	APELLIDO VARCHAR(70),
	EMAIL VARCHAR(100) NOT NULL UNIQUE,
	CLAVE VARCHAR(100),
	AVATAR VARCHAR(16),
	FKSEXO tinyint(1) UNSIGNED,
	FECHA_ALTA DATETIME,
	FOREIGN KEY(FKSEXO) REFERENCES sexos(IDSEXO) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE=innoDB;


CREATE TABLE entradas(
    ID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    TITULO VARCHAR(40),
    PREVIEW VARCHAR(150),
    TEXTO TEXT,
    THUB VARCHAR(50),
    FECHA TIMESTAMP,
    FKUSUARIOS tinyint(2),
    FKCATEGORIAS tinyint(1),
    FOREIGN KEY(FKUSUARIOS) REFERENCES usuarios(ID) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY(FKCATEGORIAS) REFERENCES categorias(ID) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE=MyISAM;

CREATE TABLE rel_post_usuario_review( 
	ID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	FKENTRADAS INT UNSIGNED,
	FKUSUARIO INT UNSIGNED,
	ESTRELLAS TINYINT(1) UNSIGNED,
	COMENTARIO VARCHAR(255),
	FECHA_ALTA DATETIME,
	
	FOREIGN KEY(FKENTRADAS) REFERENCES entradas(ID) ON DELETE SET NULL ON UPDATE CASCADE,
	FOREIGN KEY(FKUSUARIO) REFERENCES usuarios(ID) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=MyISAM;
/*Carga de datos*/



/*Categorias*/
INSERT INTO categorias (NOMBRE)
VALUES ('Comida saludable'), ('Comidas rapidas'), ('Ensaladas'), ('Postres');
/*sexo*/
INSERT INTO sexos (SEXO)
VALUES ('Hombre'),('Mujer');
INSERT INTO usuarios 
VALUES 
(NULL , 'willy', 'cordon', 'cordonwilly24@gmail.com', 'willy', NULL,1,NOW()),
(NULL , 'pepe', 'pompin', 'p@p.com', 'willy', NULL,1,NOW()),
(NULL , 'alvaro', 'cordon', 'alvaro@gmail.com', 'willy', NULL,1,NOW()),
(NULL , 'mia', 'kalifa', 'ladeloslentes@gmail.com', 'willy', NULL,2,NOW()),
(NULL , 'helio', 'pez', 'eldelgorro@gmail.com', 'willy', NULL,1,NOW());
/*entradas*/
INSERT INTO 
	entradas (TITULO, PREVIEW, TEXTO, THUB, FECHA, FKCATEGORIAS, FKUSUARIOS)
VALUES
	('Gazpacho' , 'El gazpacho tradicional no es solo una receta imprescindible en todo recetario casero que se precie..', 'El gazpacho tradicional no es solo una receta imprescindible en todo recetario casero que se precie, es que además es sencillísimo de preparar. No requiere cocción de ningún tipo, se puede ajustar la receta al gusto personal y es un plato muy versátil para incorporar a cualquier menú. La clave de un buen gazpacho reside en la calidad de los ingredientes, y si nos ajustamos al sabor más tradicional no hacer experimentos demasiado extraños. Me gustaría tomar gazpacho todo el año, aunque reconozco que en los meses cálidos sabe mejor, porque es refrescante pero también porque el sabor de los tomates marca la diferencia. ¿Cómo evitar que se vuelva aburrido? Jugando con la guarnición.', '1.jpg', NOW( ), 1, 2),
    ('Baguette de desayuno' , 'Baguette de desayuno horneada con bacon, huevo y queso', 'Quien dice desayuno dice brunch, almuerzo o incluso merienda o cena. Inspirada en los desayunos ingleses más tradicionales, esta receta de baguette horneada con bacon, huevo y queso es una elaboración sencillísima pero de lo más satisfactoria, con ingredientes básicos que combinan para crear un plato energético e irresistible para estómagos hambrientos. El pan tipo baguette funciona como base, a modo de barco, que se vacía en parte para rellenar con el resto de ingredientes. Después hay que cocinarlo en el horno unos pocos minutos para disfrutar de un interior cremoso y sabrosísimo, con la mezcla cuajada de huevos y el queso derretido.', '1.jpg', NOW( ), 1, 1),
    ('Tarta tatin de tomates' , 'Tarta tatin de tomates y queso fresco de cabra', 'Si buscáis una receta con la que impresionar a los invitados en casa, pero no tenéis ni idea de cocinar, esta deliciosa tarta tatin de tomates y queso fresco de cabra es la opción ideal. La presentación es de lujo y el sabor aún mejor, y da la sensación de haber puesto mucho esfuerzo en la cocina cuando en realidad es una elaboración sencilla, rápida y que no requiere gran talento culinario. Estamos además ya en época de buenos tomates, algo fundamental para que el sabor final de la tarta tatin sea un verdadero éxito. Sólo hay que cubrir una fuente con tomates, hornearlos, añadir el queso, cubrir con hojaldre y volver a hornear. Después lo único que hay que hacer es desmoldar colocando encima un plato grande y girando la fuente, ya que para facilitar más la tarea la tatin debe quedar al revés, con los tomates encima. Más fácil imposible.', '1.jpg', NOW( ), 2, 2),
    ('Tortilla' , 'Tortilla de espárragos trigueros', 'Aprovechando otro producto de temporada, los espárragos trigueros, podemos preparar uno de los platos más sencillos y ricos que todo el mundo debería saber preparar en casa, la tortilla. Puede que la clásica española de patatas requiera algo más de técnica y esfuerzo, pero una tortilla estilo francesa con verduras es mucho más fácil y nos solucionará un almuerzo o cena en un momento. Lo único que debemos tener en cuenta para que cocinar esta tortilla sea coser y cantar, es disponer de una buena sartén antiadherente. Es lo que suelo recomendar a todo el mundo que cocina poco en casa, porque con ella se preparan muchos platos fáciles y rápidos. Así, y controlando que la temperatura del fuego no sea muy alta, tendremos una tortilla perfecta sin esfuerzo.', '1.jpg', NOW( ), 2, 1),
    ('Flatbread' , 'Flatbread o pan plano crujiente de patata, Gruyère y crema agria', 'Las pizzas y tartas saladas caseras son una verdadera delicia que merece la pena aprender a preparar, pero reconozco que son recetas algo más elaboradas y no siempre fáciles para todo el mundo. Las masas refrigeradas son una buena alternativa a tener que recurrir a servicios a domicilio, pero podemos probar también con opciones de panes planos mucho más sencillos. Se conoce como flatbread a ese tipo de masas que son planitas pero no implican necesariamente amasados y levados más complejos. Lo bueno de este [pan plano crujiente de patata, Gruère y crema agria](Flatbread o pan plano crujiente de patata, Gruyère y crema agria) es que solo necesitamos mezclar bien, aplanar con un rodillo, y repartir encima directamente los ingredientes. Si además tenemos una mandolina no tardaremos nada en cortar la patata. El resultado final, aunque no comparable con una buena pizza, merece mucho la pena.', '1.jpg', NOW( ), 1, 3),
	('Ensalada' , 'Ensalada de lentejas con vinagreta de mostaza', 'Un gran problema de los que no saben cocinar o no tienen mucho tiempo es que suelen descuidar el consumo de legumbres. Por suerte hay conservas más que decentes con platos tradicionales que no hay más que abrir y calentar, pero ya que viene el buen tiempo nosotros apostamos por las ensaladas. Sí, se pueden usar lentejas compradas ya cocidas, pero elegid una buena marca de confianza y lavadlas y escurridlas bien. Claro que se puede hacer esta ensalada de lentejas con vinagreta de mostaza cociendo la legumbre en casa, variedades como la de Puy o la Beluga apenas necesitan tiempo de cocción, animáos a probadlas.', '1.jpg', NOW( ), 3, 4),
	('Ensalada templada' , 'Ensalada templada de patatas y tomates secos', 'Las patatas son un buen comodín para multitud de platos y para todas las ocasiones, no en vano fueron la base de la alimentación de tanta gente con pocos recursos durante siglos. Pero además podemos preparar platos muy fáciles y riquísimos con ellas, como esta ensalada templada de patatas y tomates secos, realmente sabrosa. La única cocción que se requiere es la de la propia patata, que haremos al vapor ya pelada y cortada. Aunque no es imprescindible, esta receta pide dejarlas unos minutos a remojo con azafrán, veréis que marca la diferencia de sabor final. Esther nos aconseja servirla templada como guarnición, pero fría, si la hemos hecho con antelación, también estará muy buena.', '1.jpg', NOW( ), 3, 2),
	('Bocaditos' , 'Bocaditos de hojaldre y dulce de boniato al agua de azahar', 'El día que tengáis invitados en casa a la hora del café, aprovechad para sorprender ofreciendo una bandeja de estos bocaditos de hojaldre y dulce de boniato al azahar. Son bocados golosos muy originales que os harán quedar como grandes reposteros, cuando en realidad son facilísimos de hacer. Y es que las masas de hojaldre refrigeradas son grandes aliadas en la cocina sin complicaciones. La tarea más elaborada es la de cocer o asar previamente los boniatos para sacar su carne, pero no es en absoluto difícil. Después se mezcla con azúcar, limón, ron y azahar, se cubre una mitad de hojaldre con la crema y se tapa con la otra parte de masa. Es mejor marcar un poco con el cuchillo las porciones para que al salir del horno sea mucho más fácil cortarlas. También podríamos usar manzana asada o calabaza cocida, si no tenemos boniato. ¿Y qué tal crema de chocolate?', '1.jpg', NOW( ), 4, 1),
	('Mug' , 'Mug cake de brownie con pepitas de chocolate', ' Estos pasteles individuales cocinados en el microondas son demasiado fáciles, toda una tentación en la que caer y repetir si somos especialmente golosos. Que nadie diga que no se le da bien la repostería: los mug cakes los puede hacer cualquiera.Su elaboración es bastante simple: medir los ingredientes, mezclar, llenar las tazas y calentar en el microondas. Se puede dejar el punto final que más se prefiera, por ejemplo con el interior ligeramente húmedo. Es una delicia cuando todavía está algo caliente, pero también se pueden disfrutar reposados y fríos, con nata montada o helado.', '1.jpg', NOW( ), 4, 5),
	('Brochetas' , 'Brochetas de solomillo de cerdo estilo tandoori', 'En casa nos gustan mucho las brochetas y pinchitos, ya sean de carne o de pescado, incluso en su versión vegetariana con tofu y verduras. Son perfectas para aprovechar la temporada de barbacoas, pero también se pueden cocinar dentro de casa utilizando una buena plancha o parrilla, incluso utilizando el horno con la función grill para dar un buen toque dorado. Una buena receta para tener como comodín son las brochetas de solomillo de cerdo estilo tandoori, que suena a plato de restaurante y en realidad es de lo más fácil y rápida. Los pasos a seguir no tienen pérdida: cortar la carne, mezclar los ingredientes de la salsa, dejar macerar y hornear. Se puede usar la misma receta para carne de pollo o pavo, incluso yo lo probaría con algún pescado firme.', '1.jpg', NOW( ), 2, 2);


