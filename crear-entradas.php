<?php 
include ('includes/redireccion.php');
include ('includes/header.php');
include ('setup/conexion.php')

?>

<div class="container pt-3">
  <div class="row justify-content-sm-center">
    <div class="col-sm-8 col-md-7">
    
      <div class="card border-info text-center">
        <div class="card-header">
           <h2>Comparta su receta</h2>
        </div>
        
        <div class="card-body">
      
       
          
          <form action="guardar-ent.php" method="POST" class="form-signin" enctype="multipart/form-data">

            <input type="text" class="form-control mb-2" placeholder="Titulo" required autofocus name="titulo" >

            <input type="text" class="form-control mb-2" placeholder="Preview" required name="preview" name="preview">

           <div class="form-control">
           <?php $sql = "SELECT * FROM categorias ORDER BY nombre DESC";
    			$categorias=mysqli_query($db, $sql);
   				 $result=array();
    			if ($categorias && mysqli_num_rows($categorias) >= 1) {
        		$result=$categorias;
                }?>
                <label for="">Selecionar Categoria</label>
               <select name="categorias">
               <?php   
					while($categoria = mysqli_fetch_assoc($categorias)):
					
                    ?>
                    <option value="<?=$categoria['ID']?>">
                    <?=$categoria['NOMBRE']?>
                </option>
                    <?php endwhile?>
               </select>
           </div>
           
           <input type="file" class="form-control mb-2" name="archivo" name="archivo">

             <textarea class="form-control" name="texto" placeholder="Message" rows="5" data-form-field="Message"></textarea>

            <button class="btn btn-lg btn-primary btn-block mb-1" type="submit">Crear receta</button>
            
          </form>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>



<?php
include ('includes/footer.php');
?>