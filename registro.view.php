<?php 
include ('includes/header.php');
include ('setup/conexion.php');
include ('setup/helpers.php');
?>




<section>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-8 col-lg-8 col-xl-6">
          <div class="row">
            <div class="col text-center">
              <h1>Registrate</h1>
            </div>
          </div>
					
           <form action="../registro.php" method="POST">
          <div class="row align-items-center">
           
            <div class=" col mt-4">
              <input type="text" class="form-control" placeholder="Nombre" name="nombre">
							<?php echo isset($_SESSION['error']) ? mostrarError($_SESSION['error'], 'name'):'';?>
            </div>
						
						<div class=" col mt-4">
              <input type="text" class="form-control" placeholder="Apellido" name="apellido">
							<?php echo isset($_SESSION['error']) ? mostrarError($_SESSION['error'], 'lastname'):'';?>
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <input type="email" class="form-control" placeholder="Email" name="email">
							<?php echo isset($_SESSION['error']) ? mostrarError($_SESSION['error'], 'email'):'';?>
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <input type="password" class="form-control" placeholder="Password" name="clave">
							<?php echo isset($_SESSION['error']) ? mostrarError($_SESSION['error'], 'password'):'';?>
            </div>
            
            
          </div>
         
              <button class="btn btn-primary mt-4" name="submit">Submit</button>
               </form>
							
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php borrarErrores();?> 
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h5 class="text-center">Ya tienes cuenta??<a href="login.view.php">Logueate</a></h5>
		</div>
	</div>
</div>

 <?php
	include ('includes/footer.php');
 ?>