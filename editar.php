<?php 
include ('includes/redireccion.php');
include ('includes/header.php');
include ('setup/conexion.php')

?>
<?php 
 
 $id=$_GET['id'];
    $sql = "SELECT * FROM ENTRADAS WHERE ID=$id";
  
    $entrada = mysqli_query($db, $sql);
    $resultado = mysqli_fetch_assoc($entrada);
     //var_dump($entrada)
 
 ?>
<div class="container pt-3">
  <div class="row justify-content-sm-center">
    <div class="col-sm-8 col-md-7">
    
      <div class="card border-info text-center">
        <div class="card-header">
           <h2>Editar <?= $resultado['TITULO']?></h2>
        </div>
        
        <div class="card-body">
      
       
       
          <form action="guardar-ent2.php" method="POST" class="form-signin" enctype="multipart/form-data">

            <input type="text" class="form-control mb-2" value="<?= $resultado['TITULO']?>" required autofocus name="titulo" >

            <input type="text" class="form-control mb-2" value="<?= $resultado['PREVIEW']?>" required name="preview" name="preview">

           <div class="form-control">
           <?php $sql = "SELECT * FROM categorias ORDER BY nombre DESC";
    			$categorias=mysqli_query($db, $sql);
   				 $result=array();
    			if ($categorias && mysqli_num_rows($categorias) >= 1) {
        		$result=$categorias;
                }?>
                <label for="">Selecionar Categoria</label>
               <select name="categorias">
               <?php   
					while($categoria = mysqli_fetch_assoc($categorias)):
					
                    ?>
                    <option value="<?=$categoria['ID']?>">
                    <?=$categoria['NOMBRE']?>
                </option>
                    <?php endwhile?>
               </select>
           </div>
           
           <input type="file" class="form-control mb-2" name="archivo" name="archivo">

             <textarea class="form-control" name="texto" rows="8" data-form-field="Message"><?= $resultado['TEXTO']?>"</textarea>

             <input type="hidden" name="id" value="<?php echo $id; ?>" />
             
            <button class="btn btn-lg btn-primary btn-block mb-1" type="submit">Editar receta</button>
            
          </form>
          <br>
        </div>
      </div>
    </div>
  </div>
</div>



<?php
include ('includes/footer.php');
?>