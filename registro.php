<?php
if(isset($_POST)){
    //Incluimos la conexion
include ('setup/conexion.php');
if(!isset($_SESSION)){
   session_start();
}

    $name =isset($_POST['nombre']) ? mysqli_real_escape_string($db, $_POST['nombre']) : false;
    $lastname = isset($_POST['apellido']) ?  mysqli_real_escape_string($db, $_POST['apellido']) : false;
    $email = isset( $_POST['email']) ?  mysqli_real_escape_string($db, trim($_POST['email'])) : false;
    $password = isset($_POST['clave']) ?  mysqli_real_escape_string($db, $_POST['clave']) : false;

//Errores
    $error= array();
//Validamos los campos que inserte el usuario
    if (!empty($name) && !is_numeric($name) && !preg_match("/[0-9]/",$name)) {
        $name_validate = true;
    }else{
        $name_validate = false;
        $error['name'] = 'El nombre que ingresaste no es valido';
    }
    if (!empty($lastname) && !is_numeric($lastname) && !preg_match("/[0-9]/",$lastname)) {
        $lastname_validate = true;
    }else{
        $lastname_validate = false;
        $error['lastname'] = 'El apellido que ingresaste no es valido';
    }
    if (!empty($name) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $email_validate = true;
    }else{
        $emal_validate = false;
        $error['email'] = 'El email es incorrecto';
    }
    if (!empty($password)) {
        $password_validate = true;
    }else{
        $password_validate = false;
        $error['password'] = 'La contraseña no cumple con nuestras politicas de seguridad';
    }
//
$save_user=false;
    if(count($error) == 0){
        $save_user=true;
        //cifrar password
        $password_segura = password_hash($password, PASSWORD_BCRYPT,['cost' => 4]);
        //Insertamos los usuarios
        $sql = "INSERT INTO usuarios VALUES(null, '$name', '$lastname', '$email', '$password_segura',null,null,now())"; 
        $guardar = mysqli_query($db, $sql);
        //var_dump(mysqli_error($db));
        //die();
        
        if($guardar){
            $_SESSION['completado'] = "El registro se ha completado con exito, Logueate para disfrutar de los beneficios";
            
        }else{
            $_SESSION['error']['general']="Fallo al insertar usuario";
        }

    }else{
        $_SESSION['error']=$error;
        
    }
    

}

header('Location: login.view.php');
